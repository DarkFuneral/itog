package com.example.itog.Controller;

import com.example.itog.model.Role;
import com.example.itog.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@Controller
@PreAuthorize("hasAnyAuthority('User', 'Admin')")
@RequestMapping("/roles")
public class RoleController {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleController(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    @GetMapping
    public String listPayments(Model model) {
        List<Role> roles = roleRepository.findAll();
        model.addAttribute("roles", roles);
        return "roles/list"; // Замените на имя вашего Thymeleaf-шаблона
    }

    @GetMapping("/add")
    public String showAddPaymentForm(Model model) {
        model.addAttribute("role", new Role());
        return "roles/add";
    }

    @PostMapping("/add")
    public String addPayment(@Valid @ModelAttribute("role") Role role, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "roles/add"; // Останется на странице добавления с отображением ошибок
        }
        roleRepository.save(role);
        return "redirect:/roles";
    }

    @GetMapping("/edit/{id}")
    public String showEditPaymentForm(@PathVariable("id") Long id, Model model) {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid role Id:" + id));
        model.addAttribute("role", role);
        return "roles/edit"; //
    }

    @PostMapping("/edit/{id}")
    public String editPayment(@PathVariable("id") Long id, @Valid @ModelAttribute("role") Role role, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            role.setId(id); // Set the ID back to the model to render the edit form correctly.
            return "roles/edit"; // Останется на странице редактирования с отображением ошибок
        }

        roleRepository.save(role);
        return "redirect:/roles"; // Перенаправление на страницу со всеми платежами
    }

    @GetMapping("/delete/{id}")
    public String deletePayment(@PathVariable("id") Long id) {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid role Id:" + id));

        roleRepository.delete(role);
        return "redirect:/roles"; // Перенаправление на страницу со всеми платежами
    }
}