package com.example.itog.model;


import javax.persistence.*;
import java.util.Set;

@Entity
public class ModelUser {
    public ModelUser(){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ID_User;
    private String username;
    private String password;
    private boolean active;

    @ManyToOne
    private Role role;

    public Long getID_User() {
        return ID_User;
    }

    public void setID_User(Long ID_User) {
        this.ID_User = ID_User;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Role getRoles() {
        return role;
    }

    public void setRoles(Role roles) {
        this.role = roles;
    }

    public void modelUser(String username, String password, boolean active, Role roles) {
        this.username = username;
        this.password = password;
        this.active = active;
        this.role = roles;
    }
}
